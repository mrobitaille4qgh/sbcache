package com.example.sbcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SbCacheApplication {

  public static void main(String[] args) {
    SpringApplication.run(SbCacheApplication.class, args);
  }
}
